#ifndef BUILD_H
#define BUILD_H

enum class BuildProp
{
    VERSION, COMMIT
};

const char* treetrace_buildprop(BuildProp);

#endif
