#include "common.h"

#include <linux/limits.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include <fstream>
#include <streambuf>
#include <sstream>

using namespace std;

void throw_errno()
{
    throw system_error(errno, system_category());
}

string read_file(const string& path)
{
    ifstream in(path);
    string str;

    str.assign(
        (istreambuf_iterator<char>(in)),
        istreambuf_iterator<char>()
    );

    return move(str);
}

const char* strsignal_r(int signal)
{
	return sigdescr_np(signal);
}

string readlink(const string& link_path)
{
    char path_buf[PATH_MAX];
    ssize_t rt = readlink(link_path.c_str(), path_buf, PATH_MAX);
    if (rt < 0)
        throw_errno();
    return string(path_buf, rt);
}

string bash_escape_str(const string& str)
{
    ostringstream os;

    os << '\'';
    for(char c : str)
    {
        if (c == '\'')
            os << '\\';
        os << c;
    }
    os << '\'';

    return os.str();
}
