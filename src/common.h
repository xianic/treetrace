#ifndef COMMON_H
#define COMMON_H

#include <string.h>

#include <string>
#include <system_error>

void throw_errno() __attribute__((noreturn));
std::string read_file(const std::string& path);
const char* strsignal_r(int signal);
std::string readlink(const std::string& path);
std::string bash_escape_str(const std::string& str);

#endif
