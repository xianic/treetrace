#include "configmap.h"

#include <iomanip>
#include <sstream>

namespace config
{

using namespace std;

namespace
{
    char hex_nibble(char c)
    {
        c &= 0xF;
        if (c <= 9)
            return '0' + c;
        return 'A' + c - 10;
    }

    char unhex_nibble(char c)
    {
        if (c >= '0' && c <= '9')
            return c - '0';
        if (c >= 'A' && c <= 'F')
            return c - 'A' + 10;
        if (c >= 'a' && c <= 'f')
            return c - 'a' + 10;
        throw logic_error("is not hex: " + to_string(c));
    }
}

namespace str_helper
{
    template<>
    string as_string<string>(const string& t)
    {
        return t;
    }

    template<>
    string from_string<string>(const string& t)
    {
        return t;
    }

    template<>
    string as_string<const unsigned long long>(const unsigned long long& t)
    {
        return to_string(t);
    }

    template<>
    unsigned long long from_string<unsigned long long>(const string& str)
    {
        return stoull(str);
    }

    template<>
    string as_string<const unsigned long>(const unsigned long& t)
    {
        return to_string(t);
    }

    template<>
    unsigned long from_string<unsigned long>(const string& str)
    {
        return stoul(str);
    }
}

const map<const char, const string> ConfigItem::_encodings {
    {'\0', "\\0"},
    {'\r', "\\r"},
    {'\n', "\\n"},
    {'\t', "\\t"}
};

const map<const char, const char> ConfigItem::_decodings {
    {'0', '\0'},
    {'r', '\r'},
    {'n', '\n'},
    {'t', '\t'}
};

const ConfigItem ConfigList::_none;

std::string ConfigItem::value_encoded() const
{
    return encode(_value);
}

void ConfigItem::value_encoded(const string& str)
{
    _value = decode(str);
}

std::string ConfigItem::encode(const std::string& str)
{
    ostringstream os;
    os << hex;

    for (const char& c : str)
    {
        switch (c)
        {
        case '\0':
            os << "\\0";
            break;

        case '\\':
        case '[':
        case ']':
        case '=':
            os << '\\' << c;
            break;

        default:
            if (c < ' ')
            {
                if (_encodings.count(c))
                    os << _encodings.find(c)->second;
                else
                    os << "\\x" << hex_nibble(c >> 4) << hex_nibble(c);
            } else
                os << c;
            break;
        }
    }

    return os.str();
}

std::string ConfigItem::decode(const std::string& str)
{
    enum class State {PLAIN, ESCAPE, HEX1, HEX2};
    State state = State::PLAIN;
    char nibble = 0;
    string out;

    for (char c : str)
    {
        switch(state)
        {
        case State::PLAIN:
            if (c == '\\')
                state = State::ESCAPE;
            else
                out += c;
            break;

        case State::ESCAPE:
            if (c == 'x')
                state = State::HEX1;
            else
            {
                if (_decodings.count(c))
                    out += _decodings.find(c)->second;
                else
                    out += c;
                state = State::PLAIN;
            }
            break;

        case State::HEX1:
            nibble = unhex_nibble(c) << 4;
            state = State::HEX2;
            break;

        case State::HEX2:
            nibble |= unhex_nibble(c);
            out += nibble;
            state = State::PLAIN;
            break;
        }
    }

    return out;
}

} //namespace config
