#ifndef CONFIG_MAP_H
#define CONFIG_MAP_H

#include "glid.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace config
{

using std::map;
using std::string;

namespace str_helper
{
    using std::to_string;

    //To make custom types to_string()able by config classes,
    //add specialisations of this template
    template<typename T>
    string as_string(const T& t)
    {
        return to_string(std::forward<const T&>(t));
    }

    template<typename T>
    T from_string(const string& str);

    template<>
    string as_string<string>(const string& t);

    template<>
    string from_string<string>(const string& str);

    template<>
    string as_string<const unsigned long long>(const unsigned long long& t);

    template<>
    unsigned long long from_string<unsigned long long>(const string& str);

    template<>
    string as_string<const unsigned long>(const unsigned long& t);

    template<>
    unsigned long from_string<unsigned long>(const string& str);
}

class ConfigItem
{
    const static map<const char, const string> _encodings;
    const static map<const char, const char> _decodings;

    string _value;

public:
    ConfigItem() {}
    ConfigItem(const ConfigItem&) = default;
    ConfigItem(ConfigItem&&) = default;

    template<typename T>
    ConfigItem(const T& v) :
    _value(str_helper::as_string<T>(v))
    {}

    ConfigItem& operator =(const ConfigItem&) = default;
    ConfigItem& operator =(ConfigItem&&) = default;

    template<typename T>
    ConfigItem& operator =(const T& v)
    {
        _value = str_helper::as_string<T>(v);
        return *this;
    }

    const string& value() const {return _value;}
    void value(const std::string&);

    std::string value_encoded() const;
    void value_encoded(const std::string&);

    template<typename T>
    T value_as() const
    {
        return str_helper::from_string<T>(_value);
    }

    template<typename T>
    void value_as(const T& v)
    {
        _value = str_helper::as_string<T>(v);
    }

    static std::string encode(const std::string& str);
    static std::string decode(const std::string& str);
};

class ConfigList : public map<string, ConfigItem>
{
    static const ConfigItem _none;

public:
    const ConfigItem& operator [](const string& key) const
    {
        auto it{find(key)};
        if (it == cend())
            return _none;
        return it->second;
    }

    ConfigItem& operator [](const string& key)
    {
        return map<string, ConfigItem>::operator[](key);
    }
};

} //namespace config

#endif
