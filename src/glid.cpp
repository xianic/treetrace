#include "glid.h"

using namespace std;

unique_ptr<GlidGen> GlidGen::_inst;

GlidGen& GlidGen::inst()
{
    if (!_inst)
        _inst.reset(new GlidGen);

    return *_inst.get();
}

glid_t GlidGen::next()
{
    while(++_last == 0)
        ;
    return _last;
}
