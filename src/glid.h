#ifndef GLID_H
#define GLID_H

#include <memory>

typedef unsigned long long glid_t;

class GlidGen
{
    static std::unique_ptr<GlidGen> _inst;
    glid_t _last;

    GlidGen() : _last(0) {}
    GlidGen(const GlidGen&) = delete;

    GlidGen& operator ==(const GlidGen&) = delete;

public:
    static GlidGen& inst();
    glid_t next();
};

#endif
