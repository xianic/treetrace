#include "log.h"

#include <cassert>
#include <fstream>
#include <sstream>

using namespace std;

void MultiReporter::log(const ThreadEvent& trans)
{
    for (auto& logger : *this)
        logger->log(trans);
}

void MultiReporter::report(const Processes& procs)
{
    for (auto& logger : *this)
        logger->report(procs);
}
