#ifndef LOG_H
#define LOG_H

#include "process.h"

#include <iostream>
#include <fstream>
#include <memory>
#include <utility>
#include <vector>

struct Reporter : public TransitionLogger
{
    virtual void report(const Processes&) = 0;
};

class MultiReporter : public Reporter, public std::vector<std::unique_ptr<Reporter>>
{
private:
    template<typename LogType>
    class FileLogger : public LogType
    {
        std::ofstream _out;

    public:
        template<typename... Args>
        FileLogger(const std::string& path, Args&&... args) :
        LogType(_out, std::forward<Args>(args)...),
        _out(path)
        {
        }
    };

public:
    void log(const ThreadEvent&) override;
    void report(const Processes&) override;

    template<typename LogType, typename... Args>
    void add(std::ostream& default_stream, const std::string& path, Args&&... args)
    {
        if (path.empty() || path == "-")
            emplace(end(), new LogType(default_stream, std::forward<Args>(args)...));
        else
            emplace(end(), new FileLogger<LogType>(path, std::forward<Args>(args)...));
    }
};

#endif
