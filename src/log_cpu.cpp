#include "log_cpu.h"

#include <algorithm>
#include <iomanip>

using namespace std;

namespace
{
    const vector<pair<const char*, unsigned long>> units{
        {"d", 1000 * 60 * 60 * 24},
        {"h", 1000 * 60 * 60},
        {"m", 1000 * 60},
        {"s", 1000},
        {"ms", 1}
    };

    string msec_to_string(unsigned long ms)
    {
        bool set = false;
        string str;
        for (const auto& unit : units)
        {
            if (set)
                str += ' ';
            if (ms >= unit.second)
            {
                str += to_string(ms / unit.second) + unit.first;
                ms %= unit.second;
                set = true;
            }
        }

        return str;
    }

    string duration_to_string(ThreadEvent::Clock::duration d)
    {
        return msec_to_string(chrono::duration_cast<chrono::milliseconds>(d).count());
    }

    const Thread& original_invocation(const Thread& thread)
    {
        const Thread* walk = &thread;
        const ThreadFork* chainedEvent = nullptr;

        while((chainedEvent = dynamic_cast<const ThreadFork*>(&walk->origin())) != nullptr)
            walk = &chainedEvent->origin();

        return *walk;
    }

    struct Invocation : public vector<const Thread*>
    {
        const Thread* main_thread() const
        {
            if (empty())
                return nullptr;

            return *begin();
        }

        unsigned long cost() const
        {
            unsigned long c = 0;
            for (const Thread* t : *this)
            {
                if (t->snaps().size() < 2)
                    continue;

                const ProfileSnap& snap = *t->snaps().back();
                c += snap.utime() + snap.stime();
            }
            return c;
        }

        ThreadEvent::Clock::duration runtime() const
        {
            using TP = ThreadEvent::TimePoint;

            const Thread* main = main_thread();
            if (main == nullptr)
                return ThreadEvent::Clock::duration(0);

            TP begin = main->snaps().front()->when();
            TP end = main->snaps().back()->when();
            for (const Thread* thread : *this)
            {
                begin = min(begin, thread->snaps().front()->when());
                end = max(end, thread->snaps().back()->when());
            }

            return end - begin;
        }

        void write(ostream& out) const
        {
            unsigned long runtime_ms = chrono::duration_cast<chrono::milliseconds>(runtime()).count();
            unsigned long cost_ms = cost();
            out << ' ' << msec_to_string(runtime_ms) << " runtime"
                << ", " << msec_to_string(cost_ms) << " CPU time"
                << ", " << size() << " threads";
            if (runtime_ms > 0)
                out << ", " << (static_cast<double>(cost_ms * 100) / static_cast<double>(runtime_ms)) << "% CPU";
        }
    };

    class Prog : public vector<const Invocation*>
    {
        string _name;

    public:
        Prog(const string& name) :
        _name(name)
        {
        }

        const string& name() const {return _name;}

        unsigned long cost() const
        {
            unsigned long cost = 0;
            for (const Invocation* invoc: *this)
                cost += invoc->cost();
            return cost;
        }

        ThreadEvent::Clock::duration runtime() const
        {
            ThreadEvent::Clock::duration time(0);

            for (const Invocation* invoc: *this)
                time += invoc->runtime();

            return time;
        }

        bool operator <(const Prog& that) const
        {
            return this->cost() < that.cost();
        }

        void write(ostream& out) const
        {
            unsigned long runtime_ms = chrono::duration_cast<chrono::milliseconds>(runtime()).count();
            unsigned long cost_ms = cost();
            out << '`' << _name << "`"
                << ' ' << msec_to_string(runtime_ms) << " runtime"
                << ", " << msec_to_string(cost_ms) << " CPU time"
                << ", " << size() << " invocations";
            if (runtime_ms > 0)
                out << ", " << (static_cast<double>(cost_ms * 100) / static_cast<double>(runtime_ms)) << "% CPU";
        }
    };

    ostream& operator <<(ostream& out, const Prog& prog)
    {
        prog.write(out);
        return out;
    }

    ostream& operator <<(ostream& out, const Invocation& invoc)
    {
        invoc.write(out);
        return out;
    }
}

CpuReport::CpuReport(ostream& out) :
_out(out)
{
}

void CpuReport::report(const Processes& procs)
{
    map<string, map<glid_t, Invocation>> counts;
    procs.for_all(
        [&](const Thread& thread)
        {
            const Thread& invoc {original_invocation(thread)};
            counts[invoc.comm()][invoc.glid()].emplace_back(&thread);
        }
    );

    vector<Prog> progs;
    progs.reserve(counts.size());
    for (const auto& it1 : counts) {
        progs.emplace_back(it1.first);
        Prog& lastProg {progs.back()};
        lastProg.reserve(it1.second.size());

        for (const auto& it2 : it1.second)
            lastProg.push_back(&it2.second);

        sort(lastProg.rbegin(), lastProg.rend());
    }

    sort(progs.rbegin(), progs.rend());

    _out << setw(20) << "Program" << setw(20) << "Runtime" << setw(20) << "CPU time" << setw(20) << "% CPU" << endl;
    int nz_cost = 0;
    for (const auto& prog : progs)
    {
        if (prog.cost() == 0)
            break;
        ++nz_cost;

        unsigned long rt = chrono::duration_cast<chrono::milliseconds>(prog.runtime()).count();
        unsigned long cpu = prog.cost();
        _out << setw(20) << prog.front()->main_thread()->comm()
            << setw(20) << msec_to_string(rt)
            << setw(20) << msec_to_string(cpu);
        if (rt > 0)
            _out << setw(20) << (static_cast<double>(cpu * 100) / static_cast<double>(rt));
        _out << endl;
    }
    if (nz_cost < progs.size())
        _out << (progs.size() - nz_cost) << " with zero CPU use" << endl;
}
