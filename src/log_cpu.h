#ifndef REPORT_H
#define REPORT_H

#include "log.h"

class CpuReport : public Reporter
{
private:
    std::ostream& _out;

public:
    CpuReport(std::ostream&);

    void log(const ThreadEvent&) override {}
    void report(const Processes&) override;
};

#endif
