#include "log_event.h"

#include <cassert>
#include <sstream>

using namespace std;
using config::ConfigList;
using config::ConfigItem;

namespace
{
    ostream& operator <<(ostream& out, const ConfigList& config)
    {
        out << '[';
        for (const auto& it : config)
        {
            out << "[" << ConfigItem::encode(it.first) << "=" << it.second.value_encoded() << "]";
        }
        out << ']';

        return out;
    }

    istream& operator >>(istream& in, ConfigList& config)
    {
        char c = 0;
        in >> c;
        if (c != '[')
            throw ParseError("not at start of config");

        c = 0;
        in.get(c);

        while (in)
        {
            if (c == ']')
                break;

            if (c != '[')
                throw ParseError("not at start of config item");

            string key, value;
            bool inKey = true;
            bool inEscape = false;
            while (in.get(c))
            {
                string& arg{inKey ? key : value};
                if (inEscape)
                {
                    arg += c;
                    inEscape = false;
                }
                else
                {
                    if (c == ']')
                        break;
                    if (c == '=')
                    {
                        if (!inKey)
                            throw ParseError("too many unescaped '='");
                        inKey = false;
                        continue;
                    }
                    if (c == '\\')
                        inEscape = true;
                    arg += c;
                }
            }

            if (c != ']')
                throw ParseError("missing end of config item");

            config[ConfigItem::decode(key)].value_encoded(value);
            key.clear();
            value.clear();
            in.get(c);
        }

        if (c != ']')
            throw ParseError("missing end of config");

        return in;
    }

    ostream& operator <<(ostream& out, const Thread& thread)
    {
        return out << thread.as_config();
    }

    ostream& operator <<(ostream& out, const ThreadEvent::TimePoint& when)
    {
        using namespace chrono;
        return out << duration_cast<microseconds>(when.time_since_epoch()).count();
    }

    istream& operator >>(istream& in, ThreadEvent::TimePoint& when)
    {
        long long i = 0;
        in >> i;
        when = ThreadEvent::Clock::time_point() + chrono::microseconds(i);
        return in;
    }
}

EventLogger::EventLogger(ostream& out) :
_out(out)
{
}

void EventLogger::logFirst(const ThreadFirst& first)
{
    _out << first.current().tid() << ' ' << first.current();
}

void EventLogger::logExec(const ThreadExec& exec)
{
    _out << exec.origin().glid() << ' ' << exec.current();
}

void EventLogger::logFork(const ThreadFork& fork)
{
    _out << fork.origin().glid() << ' ' << fork.current().tid() << ' ' << (fork.current().is_main() ? 'p' : 't');
}

void EventLogger::logExit(const ThreadExit& exit)
{
    if (exit.signaled())
    {
        if (exit.core_dumped())
            _out << 'd';
        else
            _out << 's';
        _out << ' ' << exit.signal();
    }
    else
        _out << "c " << exit.exit_code();
}

void EventLogger::logSnap(const ProfileSnap& snap)
{
    _out << snap.as_config();
}

void EventLogger::log(const ThreadEvent& trans)
{
    _out << trans.when() << ' ' << (int)(trans.type()) << ' ' << trans.current().glid() << ' ';

    switch(trans.type())
    {
    case ThreadEventType::FIRST:
        logFirst(dynamic_cast<const ThreadFirst&>(trans));
        break;

    case ThreadEventType::FORK:
        logFork(dynamic_cast<const ThreadFork&>(trans));
        break;

    case ThreadEventType::EXEC:
        logExec(dynamic_cast<const ThreadExec&>(trans));
        break;

    case ThreadEventType::EXIT:
        logExit(dynamic_cast<const ThreadExit&>(trans));
        break;

    case ThreadEventType::SNAP:
        logSnap(dynamic_cast<const ProfileSnap&>(trans));
        break;
    }

    _out << endl;
}

EventParser::EventParser(Processes& procs) :
_procs(procs)
{
}

void EventParser::parseFirst(ThreadEvent::TimePoint when, istream& in)
{
    glid_t glid = 0;
    pid_t tid = 0;
    ConfigList cfg;

    in >> glid >> tid >> cfg;
    if(glid == 0 || tid == 0)
        throw ParseError("invalid IDs");

    _procs.transition<ThreadFirst>(when, tid, glid, cfg);
}

void EventParser::parseExec(ThreadEvent::TimePoint when, istream& in)
{
    glid_t from = 0, to = 0;
    ConfigList cfg;

    in >> to >> from >> cfg;
    if(from == 0 || to == 0)
        throw ParseError("invalid IDs");

    Thread* thread = _procs.thread_by_glid(from);
    if (thread == nullptr)
        throw ParseError("reference to undefined thread " + to_string(from));

    _procs.transition<ThreadExec>(when, *thread, to, cfg);
}

void EventParser::parseFork(ThreadEvent::TimePoint when, istream& in)
{
    glid_t to = 0, from = 0;
    pid_t tid = 0;
    char type = '?';

    in >> to >> from >> tid >> type;
    if(from == 0 || to == 0 || tid == 0 || (type != 't' && type != 'p'))
        throw ParseError("invalid fields");

    Thread* thread = _procs.thread_by_glid(from);
    if (thread == nullptr)
        throw ParseError("reference to undefined thread " + to_string(from));

    _procs.transition<ThreadFork>(when, *thread, tid, type == 'p', to);
}

void EventParser::parseExit(ThreadEvent::TimePoint when, istream& in)
{
    glid_t glid = 0;
    char type = '?';
    int code = 0;

    in >> glid >> type >> code;

    Thread* thread = _procs.thread_by_glid(glid);
    if (thread == nullptr)
        throw ParseError("reference to undefined thread " + to_string(glid));

    _procs.transition<ThreadExit>(when, *thread, type != 'c', type == 'd', code);
}

void EventParser::parseSnap(ThreadEvent::TimePoint when, std::istream& in)
{
    glid_t glid = 0;
    config::ConfigList cfg;

    in >> glid >> cfg;

    Thread* thread = _procs.thread_by_glid(glid);
    if (thread == nullptr)
        throw ParseError("reference to undefined thread " + to_string(glid));

    thread->add_snap(when, cfg);
}

void EventParser::parse(istream& in)
{
    string line;
    while (getline(in, line))
    {
        istringstream ins(line);
        ThreadEvent::TimePoint when;
        int type = -1;

        ins >> when >> type;
        switch(type)
        {
        case static_cast<int>(ThreadEventType::FIRST):
            parseFirst(when, ins);
            break;

        case static_cast<int>(ThreadEventType::EXEC):
            parseExec(when, ins);
            break;

        case static_cast<int>(ThreadEventType::FORK):
            parseFork(when, ins);
            break;

        case static_cast<int>(ThreadEventType::EXIT):
            parseExit(when, ins);
            break;

        case static_cast<int>(ThreadEventType::SNAP):
            parseSnap(when, ins);
            break;

        default:
            throw ParseError("unknown type: " + to_string(type));
        }
    }
}
