#ifndef LOG_EVENT_H
#define LOG_EVENT_H

#include "log.h"

#include <iostream>

struct ParseError : public std::runtime_error
{
    explicit ParseError(const std::string& what_arg) : std::runtime_error(what_arg) {}
    explicit ParseError(const char* what_arg ) : std::runtime_error(what_arg) {}
};

class EventLogger : public Reporter
{
    std::ostream& _out;

    void logFirst(const ThreadFirst&);
    void logExec(const ThreadExec&);
    void logFork(const ThreadFork&);
    void logExit(const ThreadExit&);
    void logSnap(const ProfileSnap&);

public:
    EventLogger(std::ostream&);

    void log(const ThreadEvent&) override;
    void report(const Processes&) override {}
};

class EventParser
{
    Processes& _procs;

    void parseFirst(ThreadEvent::TimePoint, std::istream&);
    void parseExec(ThreadEvent::TimePoint, std::istream&);
    void parseFork(ThreadEvent::TimePoint, std::istream&);
    void parseExit(ThreadEvent::TimePoint, std::istream&);
    void parseSnap(ThreadEvent::TimePoint, std::istream&);

public:
    EventParser(Processes&);

    void parse(std::istream&);
};

#endif
