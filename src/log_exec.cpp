#include "common.h"
#include "log_exec.h"

#include <set>

using namespace std;

ExecLogger::ExecLogger(ostream& out, const bool show_environ) :
_out(out),
_show_environ(show_environ),
_last_thread_seen(nullptr)
{
}

void ExecLogger::log(const Thread& thread)
{
    const ProcessFields& fields = thread.fields();

    _out << "#glid: " << thread.glid() << " " << fields.exe() << endl;

    if (!thread.snaps().empty())
    {
        ProfileSnap& snap0 = *(thread.snaps()[0]);
        _out << "cd " << bash_escape_str(snap0.cwd()) << endl;
    }

    if (_show_environ)
    {
        string env = fields.environ();
        string::size_type search_from = 0;
        set<string> vars;
        do
        {
            string::size_type search_to = env.find('\0', search_from);
            if (search_to == string::npos)
                search_to = env.size();

            if (search_from < search_to)
                vars.insert(env.substr(search_from, search_to - search_from));

            search_from = search_to + 1;
        } while (search_from < env.size());

        if (!vars.empty())
        {
            _out << "env -i \\" << endl;
            for (const auto& var : vars)
                _out << "\t" << bash_escape_str(var) << " \\" << endl;
            _out << "\\" << endl << "\t";
        }
    }

    _out << thread.cmdline().bash_escape() << endl;

    /*string cwd = fields["cwd"]
    if (!cwd.empty())
        _out << "cd " << bash_escape_str(cwd) << endl;*/
}

void ExecLogger::log(const ThreadEvent& event)
{
    if (_last_thread_seen && _last_thread_seen != &event.current())
    {
        log(*_last_thread_seen);
        _last_thread_seen = nullptr;
    }

    switch (event.type())
    {
    case ThreadEventType::EXEC:
        _last_thread_seen = &event.current();
        break;

    case ThreadEventType::SNAP:
        if (_last_thread_seen)
        {
            log(*_last_thread_seen);
            _last_thread_seen = nullptr;
        }
        break;

    default:
        break;
    }
}

void ExecLogger::report(const Processes&)
{
    if (_last_thread_seen)
    {
        log(*_last_thread_seen);
        _last_thread_seen = nullptr;
    }
}
