#ifndef LOG_EXEC
#define LOG_EXEC

#include "log.h"

class ExecLogger : public Reporter
{
    std::ostream& _out;
    const bool _show_environ;
    const Thread* _last_thread_seen;

    void log(const Thread&);

public:
    ExecLogger(std::ostream&, const bool show_environ);

    void log(const ThreadEvent&) override;
    void report(const Processes&) override;
};

#endif
