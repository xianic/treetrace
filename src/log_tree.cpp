#include "log_tree.h"

#include <cassert>
#include <sstream>

using namespace std;

namespace
{
    ostream& operator <<(ostream& out, const Thread& thread)
    {
        out << "`" << thread.comm() << "` (" << thread.tid() << "/" << thread.glid() << ")";
        return out;
    }
}

TreeLogger::Indent::Indent() :
_pid(0),
_is_main(true)
{
}

TreeLogger::Indent::Indent(const Thread& thread) :
_pid(thread.tid()),
_is_main(thread.is_main())
{
}

bool TreeLogger::Indent::empty() const
{
    return _pid == 0;
}

void TreeLogger::Indent::clear()
{
    _pid = 0;
    _is_main = true;
}

bool TreeLogger::Indent::operator ==(const Thread& thread) const
{
    return _pid == thread.tid();
}

TreeLogger::Indent& TreeLogger::Indent::operator =(const Thread& thread)
{
    _pid = thread.tid();
    _is_main = thread.is_main();
    return *this;
}

char TreeLogger::Indent::pre_char(PrintMode mode)
{
    switch (mode)
    {
    case PrintMode::FORK_FROM:
    case PrintMode::PLAIN:
        return ' ';

    case PrintMode::FORK_MID_THREAD:
    case PrintMode::FORK_TO_THREAD:
        return '-';

    case PrintMode::FORK_MID_PROC:
    case PrintMode::FORK_TO_PROC:
        return '=';

    default: // Should never reach here
        return '\0';
    }
}

void TreeLogger::Indent::print(ostream& out, PrintMode mode) const
{
    const char pc = pre_char(mode);
    out << pc << pc << pc;

    if (empty())
    {
        out << pc;
        return;
    }

    //last char
    switch (mode)
    {
    case PrintMode::FORK_FROM:
        out << '+';
        break;

    case PrintMode::PLAIN:
    case PrintMode::FORK_MID_PROC:
    case PrintMode::FORK_MID_THREAD:
        out << (_is_main ? '|' : ':');
        break;

    case PrintMode::FORK_TO_PROC:
    case PrintMode::FORK_TO_THREAD:
        out << '\\';
        break;
    }
}

TreeLogger::TreeLogger(ostream& out, bool show_threads) :
_out(out),
_show_threads(show_threads)
{
}

void TreeLogger::print_indent(int max)
{
    const int ii = _procs.size();

    if (max < 0)
        max = ii;

    for (int i = 0; i < ii && i < max; ++i)
        _procs[i].print(_out);
}

void TreeLogger::print_indent_fork(int from, int to, bool is_main)
{
    int ii = _procs.size() - 1;
    if (ii > to)
        ii = to;

    for (int i = 0; i <= ii; ++i)
    {
        PrintMode mode;
        if (i < from)
            mode = PrintMode::PLAIN;
        else if (i == from)
            mode = PrintMode::FORK_FROM;
        else if (i < to)
            mode = (is_main ? PrintMode::FORK_MID_PROC : PrintMode::FORK_MID_THREAD);
        else
            mode = (is_main ? PrintMode::FORK_TO_PROC : PrintMode::FORK_TO_THREAD);

        _procs[i].print(_out, mode);
    }
}

int TreeLogger::indent_of(const Thread& thread, int min_place)
{
    const int ii = _procs.size();
    for (int i = 0; i < ii; ++i)
        if (_procs[i] == thread)
            return i;

    for (int i = min_place; i < ii; ++i)
        if (_procs[i].empty())
        {
            _procs[i] = thread;
            return i;
        }

    _procs.emplace_back(thread);
    return ii;
}

void TreeLogger::log_fork(const ThreadFork& trans)
{
    const Thread& origin{_show_threads ? trans.origin() : trans.origin().main_thread()};
    int oindent = indent_of(origin);
    int findent = indent_of(trans.current(), oindent);

    print_indent_fork(oindent, findent, trans.current().is_main());
    _out << ' ' << trans.current() << endl;

    print_indent();
    _out << endl;
}

void TreeLogger::log_exit(const ThreadExit& trans)
{
    int index = indent_of(trans.current());

    print_indent(index);
    _out << "  ";
    if (trans.signaled())
    {
        _out << "exited signal " << trans.signal() << " (" << trans.signal_desc() << ")";
        if (trans.core_dumped())
            _out << " core dumped";
    }
    else
        _out << "exited code " << trans.exit_code();

    _out << " " << trans.current() << endl;

    _procs[index].clear();
    while (!_procs.empty() && _procs[_procs.size() - 1].empty())
        _procs.erase(_procs.end() - 1);

    print_indent();
    _out << endl;
}

void TreeLogger::log_first(const ThreadFirst& trans)
{
    int index = indent_of(trans.current());

    print_indent(index);
    _out << ' ' << trans.current() << endl;
    print_indent();
    _out << endl;
}

void TreeLogger::log_exec(const ThreadExec& trans)
{
    const Thread& show {trans.current()};
    int index = indent_of(show);

    print_indent(index);
    _out << ' ' << show << ' ' << show.cmdline().bash_escape() << endl;
    print_indent();
    _out << endl;
}

void TreeLogger::log(const ThreadEvent& trans)
{
    if (!_show_threads && !trans.current().is_main())
        return;

    switch(trans.type())
    {
    case ThreadEventType::FIRST:
        log_first(dynamic_cast<const ThreadFirst&>(trans));
        break;

    case ThreadEventType::EXEC:
        log_exec(dynamic_cast<const ThreadExec&>(trans));
        break;

    case ThreadEventType::FORK:
        log_fork(dynamic_cast<const ThreadFork&>(trans));
        break;

    case ThreadEventType::EXIT:
        log_exit(dynamic_cast<const ThreadExit&>(trans));
        break;
    }
}
