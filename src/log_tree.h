#ifndef LOG_TREE_H
#define LOG_TREE_H

#include "log.h"

#include <iostream>
#include <vector>

class TreeLogger : public Reporter
{
    enum class PrintMode {PLAIN, FORK_FROM, FORK_MID_THREAD, FORK_MID_PROC, FORK_TO_THREAD, FORK_TO_PROC};

    class Indent
    {
        pid_t _pid;
        bool _is_main;

        static char pre_char(PrintMode mode);

    public:
        Indent();
        Indent(const Thread&);

        bool operator ==(const Thread&) const;
        bool empty() const;

        Indent& operator =(const Thread&);
        void clear();

        void print(std::ostream&, PrintMode = PrintMode::PLAIN) const;
    };

    std::ostream& _out;
    std::vector<Indent> _procs;
    const bool _show_threads;

    int indent_of(const Thread&, int min_place = 0);
    void print_indent(int max = -1);
    void print_indent_fork(int from, int to, bool is_main);
    void log_first(const ThreadFirst&);
    void log_fork(const ThreadFork&);
    void log_exec(const ThreadExec&);
    void log_exit(const ThreadExit&);

public:
    TreeLogger(std::ostream& out, bool show_threads);

    void log(const ThreadEvent&) override;
    void report(const Processes&) override {}
};

#endif
