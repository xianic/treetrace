#include "build.h"
#include "common.h"
#include "log_event.h"
#include "log_exec.h"
#include "log_tree.h"
#include "log_cpu.h"
#include "process.h"
#include "tracer.h"

#include "libgen.h"
#include "unistd.h"

#include <fstream>
#include <iostream>

using namespace std;

void usage(char* me)
{
    me = basename(me);
    cout << "Usage:" << endl
        << " " << me << " [options] [--] command to trace" << endl
        << " " << me << " -i log_file [options]" << endl
        << endl
        << "Time and trace the execution of a process tree" << endl
        << endl
        << "Options:" << endl
        << " -i <file>                     Read log file instead of executing a program" << endl
        << "-u user                        Switch to another user before tracing" << endl
        << "-v                             Print version and exit" << endl
        << endl
        << "Reports:" << endl
        << " -t <file>                     Write ascii tree to <file>" << endl
        << " -l <file>                     Write event log to <file>. Use -E to" << endl
        << "                               capture environment." << endl
        << " -e <file>                     Write command lines exected to <file>." << endl
        << "                               Use -E to print environment" << endl
        << "    note: the report options can be used multiple time to generate multiple" << endl
        << "    reports" << endl
        << " -c <file>                     Processes by CPU usage" << endl
        << endl
        << "Report Options:" << endl
        << " -T                            Show threads (this only affects the tree report" << endl
        << "                               (-t)" << endl
        << " -E                            Capture process environments. This will significantly" << endl
        << "                               increase the size of log reports (-l)" << endl;
    exit(1);
}

void show_version()
{
    cout << "treetrace " << treetrace_buildprop(BuildProp::VERSION) << endl;
    string build = treetrace_buildprop(BuildProp::COMMIT);
    if (!build.empty())
            cout << "build: " << build << endl;
}

int main(int argc, char* argv[])
{
    const char* optstring = "t:l:i:e:TEc:u:v";
    MultiReporter reporters;
    string infile;
    string switch_user;
    bool show_threads = false;
    bool capture_environ = false;

    ostream out(cout.rdbuf());

    int c;
    while ((c = getopt(argc, argv, optstring)) != -1)
    {
        switch (c)
        {
        case 'T':
            show_threads = true;
            break;

        case 'E':
            capture_environ = true;
            break;

        case 't':
            reporters.add<TreeLogger>(out, optarg, show_threads);
            break;

        case 'l':
            reporters.add<EventLogger>(out, optarg);
            break;

        case 'e':
            reporters.add<ExecLogger>(out, optarg, capture_environ);
            break;

        case 'i':
            infile = optarg;
            break;

        case 'c':
            reporters.add<CpuReport>(out, optarg);
            break;

        case 'u':
            switch_user = optarg;
            break;

        case 'v':
            show_version();
            return 0;

        case '?':
            usage(argv[0]);
            return 1;
        }
    }

    if (infile.empty() == (optind >= argc))
        usage(argv[0]);

    if (reporters.empty())
        reporters.add<TreeLogger>(out, "-", show_threads);

    try {
        Processes procs(&reporters);
        if (infile.empty())
        {
            out.rdbuf(cerr.rdbuf());
            trace_exec(procs, &argv[optind], switch_user, capture_environ);
        }
        else
        {
            EventParser parser(procs);
            if (infile == "-")
                parser.parse(cin);
            else
            {
                ifstream in(infile);
                parser.parse(in);
            }
        }

        reporters.report(procs);

        return 0;
    }
    catch (const system_error& e) {
        cout << "error: " << e.what() << ", code=" << e.code() << endl;
    } catch (const exception& e) {
        cout << "exception: " << e.what() << endl;
    }

    return 1;
}
