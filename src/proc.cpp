#include "common.h"
#include "proc.h"

#include <fstream>
#include <sstream>

using namespace std;

ProcStat::ProcStat(pid_t tgid0, pid_t tid0) :
tgid(tgid0),
tid(tid0)
{
    string path {"/proc/" + to_string(tgid) + "/task/" + to_string(tid) + "/stat"};
    string line{read_file(path)};

    /*
     * The comm field may contain spaces and '(' or ')' chars. It is
     * also surrounded by '(' and ')' and no other fields contain these.
     */
    string::size_type comm_begin = line.find_first_of('(');
    string::size_type comm_end = line.find_last_of(')');

    if (
        comm_begin == string::npos ||
        comm_end == string::npos ||
        comm_begin >= comm_end ||
        comm_begin < 1 ||
        comm_end >= line.size() - 2)
        throw logic_error("no comm");

    push_back(line.substr(0, comm_begin - 1));
    push_back(line.substr(comm_begin + 1, comm_end - comm_begin - 1));

    istringstream in(line.substr(comm_end + 2));

    //read all remaining fields
    string field;
    while(getline(in, field, ' '))
    {
        push_back(field);
        field = "";
    }

    string& last {*(end() - 1)};
    if (!last.empty() && last.at(last.size() - 1) == '\n')
        last = last.substr(0, last.size() - 1);
}

ProcIO::ProcIO(pid_t tgid, pid_t tid)
{
    map<string, unsigned long> values;
    string line;
    ifstream in("/proc/" + to_string(tgid) + "/task/" + to_string(tid) + "/io");
    while(getline(in, line))
    {
        string::size_type offset = line.find(": ");
        if (offset == string::npos)
            continue;
        values[line.substr(0, offset)] = stoul(line.substr(offset + 2));
    }

    rchar = values["rchar"];
    wchar = values["wchar"];
    syscr = values["syscr"];
    syscw = values["syscw"];
    read_bytes = values["read_bytes"];
    write_bytes = values["write_bytes"];
    cancelled_write_bytes = values["canceled_write_bytes"];
}

ProcIO::ProcIO(const config::ConfigList& values)
{
    rchar = values["rchar"].value_as<unsigned long>();
    wchar = values["wchar"].value_as<unsigned long>();
    syscr = values["syscr"].value_as<unsigned long>();
    syscw = values["syscw"].value_as<unsigned long>();
    read_bytes = values["read_bytes"].value_as<unsigned long>();
    write_bytes = values["write_bytes"].value_as<unsigned long>();
    cancelled_write_bytes = values["canceled_write_bytes"].value_as<unsigned long>();
}

void ProcIO::add_to_config(config::ConfigList& values) const
{
    values["rchar"] = rchar;
    values["wchar"] = wchar;
    values["syscr"] = syscr;
    values["syscw"] = syscw;
    values["read_bytes"] = read_bytes;
    values["write_bytes"] = write_bytes;
    values["canceled_write_bytes"] = cancelled_write_bytes;
}

string proc_environ(pid_t tgid, pid_t tid)
{
    return read_file({"/proc/" + to_string(tgid) + "/task/" + to_string(tid) + "/environ"});
}
