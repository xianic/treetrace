#ifndef PROC_H
#define PROC_H

#include "configmap.h"

#include <map>
#include <string>
#include <vector>

#include <sys/types.h>

class ProcStat : public std::vector<std::string>
{
public:
    enum StatField : std::vector<std::string>::size_type
    {
        PID = 0,
        COMM = 1,
        STATE = 2,
        PPID = 3,
        PGRP = 4,
        SESSION = 5,
        TTY_NR = 6,
        TPGID = 7,
        FLAGS = 8,
        MINFLT = 9,
        CMINFLT = 10,
        MAJFLT = 11,
        CMAJFLT = 12,
        UTIME = 13,
        STIME = 14,
        CUTIME = 15,
        CSTIME = 16,
        PRIORITY = 17,
        NICE = 18,
        NUM_THREADS = 19,
        ITREALVALUE = 20,
        STARTTIME = 21,
        VSIZE = 22,
        RSS = 23,
        RSSLIM = 24,
        STARTCODE = 25,
        ENDCODE = 26,
        STARTSTACK = 27,
        KSTKESP = 28,
        KSTKEIP = 29,
        SIGNAL = 30,
        BLOCKED = 31,
        SIGIGNORE = 32,
        SIGCATCH = 33,
        WCHAN = 34,
        NSWAP = 35,
        CNSWAP = 36,
        EXIT_SIGNAL = 37,
        PROCESSOR = 38,
        RT_PRIORITY = 39,
        POLICY = 40,
        DELAYACCT_BLKIO_TICKS = 41,
        GUEST_TIME = 42,
        CGUEST_TIME = 43,
        START_DATA = 44,
        END_DATA = 45,
        START_BRK = 46,
        ARG_START = 47,
        ARG_END = 48,
        ENV_START = 49,
        ENV_END = 50,
        EXIT_CODE = 51
    };

    ProcStat(pid_t tgid, pid_t tid);

    const pid_t tgid;
    const pid_t tid;
    std::string field(StatField) const;
    unsigned long field_ul(StatField) const;
};

struct ProcIO
{
    ProcIO(pid_t tgid, pid_t tid);
    ProcIO(const config::ConfigList&);
    ProcIO(const ProcIO&) = default;

    unsigned long rchar;
    unsigned long wchar;
    unsigned long syscr;
    unsigned long syscw;
    unsigned long read_bytes;
    unsigned long write_bytes;
    unsigned long cancelled_write_bytes;

    void add_to_config(config::ConfigList& cfg) const;
};

std::string proc_environ(pid_t tgid, pid_t tid);

#endif
