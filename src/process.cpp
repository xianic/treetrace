#include "common.h"
#include "process.h"

#include <sys/wait.h>
#include <unistd.h>

#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;

namespace config::str_helper
{
    template<>
    string as_string(const CommandLine& cmdline)
    {
        string out;
        bool first = true;
        for (const string& arg : cmdline)
        {
            if (first)
                first = false;
            else
                out += '\0';
            out += arg;
        }

        return out;
    }

    template<>
    CommandLine from_string(const string& str)
    {
        return CommandLine(str);
    }
}

ThreadEvent::ThreadEvent(ThreadEventType type, TimePoint when) :
_type(type),
_when(when)
{
}

ChainedThreadEvent::ChainedThreadEvent(ThreadEventType type, TimePoint when) :
ThreadEvent(type, when)
{
}

ThreadEvent::~ThreadEvent()
{
}

ThreadFirst::ThreadFirst(Processes& procs, ThreadEvent::TimePoint when, pid_t tid, const bool capture_environ) :
ThreadEvent(ThreadEventType::FIRST, when),
_fields(ProcStat(tid, tid), capture_environ),
_current(procs, *this, tid, _fields)
{
}

ThreadFirst::ThreadFirst(Processes& procs, ThreadEvent::TimePoint when, pid_t tid, glid_t glid, const config::ConfigList& cfg) :
ThreadEvent(ThreadEventType::FIRST, when),
_fields(cfg),
_current(procs, *this, tid, _fields, glid)
{
}

ThreadFork::ThreadFork(Processes& procs, ThreadEvent::TimePoint when, Thread& origin, pid_t ntid, bool new_thread) :
ChainedThreadEvent(ThreadEventType::FORK, when),
_origin(origin),
_current(procs, *this, ntid, origin.fields(), new_thread)
{
}

ThreadFork::ThreadFork(Processes& procs, ThreadEvent::TimePoint when, Thread& origin, pid_t ntid, bool new_thread, glid_t glid) :
ChainedThreadEvent(ThreadEventType::FORK, when),
_origin(origin),
_current(procs, *this, ntid, origin.fields(), new_thread, glid)
{
}

ThreadExec::ThreadExec(Processes& procs, ThreadEvent::TimePoint when, Thread& origin, const bool capture_environ) :
ChainedThreadEvent(ThreadEventType::EXEC, when),
_fields(ProcStat(origin.tid(), origin.tid()), capture_environ),
_origin(origin),
_current(procs, *this, origin.tid(), _fields, true)
{
}

ThreadExec::ThreadExec(Processes& procs, ThreadEvent::TimePoint when, Thread& origin, glid_t next, const config::ConfigList& cfg) :
ChainedThreadEvent(ThreadEventType::EXEC, when),
_fields(cfg),
_origin(origin),
_current(procs, *this, _origin.tid(), _fields, true, next)
{
}

ThreadExit::ThreadExit(Processes& procs, ThreadEvent::TimePoint when, Thread& current, int wstatus) :
ThreadEvent(ThreadEventType::EXIT, when),
_current(current),
_signaled(WIFSIGNALED(wstatus)),
_dumped(_signaled ? WCOREDUMP(wstatus) : false),
_code(_signaled ? WTERMSIG(wstatus) : WEXITSTATUS(wstatus))
{
    if (!WIFSIGNALED(wstatus) && !WIFEXITED(wstatus))
        throw logic_error("not a termination");
}

ThreadExit::ThreadExit(Processes&, ThreadEvent::TimePoint when, Thread& current, bool signaled, bool dumped, int code) :
ThreadEvent(ThreadEventType::EXIT, when),
_current(current),
_signaled(signaled),
_dumped(dumped),
_code(code)
{
    if (_dumped && !_signaled)
        throw new logic_error("core dump without signal");
}

bool ThreadExit::signaled() const
{
    return _signaled;
}

int ThreadExit::signal() const
{
    if (_signaled)
        return _code;
    return 0;
}

bool ThreadExit::exited() const
{
    return !_signaled;
}

int ThreadExit::exit_code() const
{
    if (exited())
        return _code;
    return 127 + _code;
}

bool ThreadExit::core_dumped() const
{
    return _dumped;
}

const char* ThreadExit::signal_desc() const
{
    return strsignal_r(signal());
}

CommandLine::CommandLine(pid_t id) :
CommandLine(read_file("/proc/" + to_string(id) + "/cmdline"))
{
}

CommandLine::CommandLine(const string& nullSeparated)
{
    istringstream in{nullSeparated};

    string arg;
    while (getline(in, arg, '\0'))
        push_back(arg);
}

string CommandLine::bash_escape() const
{
    ostringstream os;

    bool first = true;
    for(const string& arg : *this)
    {
        if (first)
            first = false;
        else
            os << ' ';

        os << bash_escape_str(arg);
    }

    return os.str();
}

string ProcStat::field(StatField index) const
{
    if (index < 0 || index >= size())
        return "0";
    return (*this)[index];
}

unsigned long ProcStat::field_ul(StatField index) const
{
    return stol(field(index));
}

unsigned long ProfileSnap::_sc_clk_tck = sysconf(_SC_CLK_TCK);

ProfileSnap::ProfileSnap(Thread& thread) :
ProfileSnap(
    ThreadEvent::Clock::now(),
    thread,
    ProcStat(thread.main_thread().tid(), thread.tid()),
    ProcIO(thread.main_thread().tid(), thread.tid())
)
{
}

ProfileSnap::ProfileSnap(ThreadEvent::TimePoint when, Thread& thread, const ProcStat& stat, const ProcIO& io) :
ThreadEvent(ThreadEventType::SNAP, when),
_thread(thread),
_utime(1000 * stat.field_ul(ProcStat::StatField::UTIME) / _sc_clk_tck),
_stime(1000 * stat.field_ul(ProcStat::StatField::STIME) / _sc_clk_tck),
_io(io),
_cwd(readlink("/proc/" + stat.field(ProcStat::StatField::PID) + "/cwd"))
{
}

ProfileSnap::ProfileSnap(ThreadEvent::TimePoint when, Thread& thread, const config::ConfigList& cfg) :
ThreadEvent(ThreadEventType::SNAP, when),
_thread(thread),
_utime(cfg["utime"].value_as<unsigned long>()),
_stime(cfg["stime"].value_as<unsigned long>()),
_io(cfg),
_cwd(cfg["cwd"].value())
{
}

config::ConfigList ProfileSnap::as_config() const
{
    config::ConfigList list;
    list["utime"] = _utime;
    list["stime"] = _stime;
    _io.add_to_config(list);
    list["cwd"] = _cwd;
    return list;
}

ProcessFields::ProcessFields(const ProcStat& stat, const bool capture_environ) :
ProcessFields(stat, CommandLine(stat.tid), capture_environ)
{
}

ProcessFields::ProcessFields(const ProcStat& stat, CommandLine&& cmdline, const bool capture_environ) :
_cmdline(move(cmdline)),
_comm(stat.field(ProcStat::StatField::COMM))
{
    _exe = readlink("/proc/" + stat.field(ProcStat::StatField::PID) + "/exe");
    if (capture_environ)
        _environ = proc_environ(stat.tgid, stat.tid);
}

ProcessFields::ProcessFields(const config::ConfigList& list) :
_cmdline(list["cmdline"].value_as<CommandLine>()),
_comm(list["comm"].value()),
_exe(list["exe"].value()),
_environ(list["environ"].value())
{
}

Thread::Thread(Processes& procs, ChainedThreadEvent& origin, pid_t tid, const ProcessFields& fields, bool new_process) :
_glid(GlidGen::inst().next()),
_tid(tid),
_procs(procs),
_fields(fields),
_proc(new_process ? procs.process_create(*this) : origin.origin().process()),
_origin(origin),
_demise(nullptr)
{
}

Thread::Thread(Processes& procs, ChainedThreadEvent& origin, pid_t tid, const ProcessFields& fields, bool new_process, glid_t glid) :
_glid(glid),
_tid(tid),
_procs(procs),
_fields(fields),
_proc(new_process ? procs.process_create(*this) : origin.origin().process()),
_origin(origin),
_demise(nullptr)
{
}

Thread::Thread(Processes& procs, ThreadFirst& origin, pid_t tid, const ProcessFields& fields) :
_glid(GlidGen::inst().next()),
_tid(tid),
_procs(procs),
_fields(fields),
_proc(procs.process_create(*this)),
_origin(origin),
_demise(nullptr)
{
}

Thread::Thread(Processes& procs, ThreadFirst& origin, pid_t tid, const ProcessFields& fields, glid_t glid) :
_glid(glid),
_tid(tid),
_procs(procs),
_fields(fields),
_proc(procs.process_create(*this)),
_origin(origin),
_demise(nullptr)
{
}

Thread::~Thread()
{
}

void Thread::demise(const ThreadEvent& demise)
{
    if (_demise)
        throw logic_error("process already ended");

    _demise = &demise;
}

bool Thread::is_main() const
{
    return _glid == _proc.glid();
}

const Thread& Thread::main_thread() const
{
    return _proc.main_thread();
}

void Thread::add_snap()
{
    _snaps.emplace_back(make_unique<ProfileSnap>(*this));
    _procs.log(*_snaps.back());
}

void Thread::add_snap(ThreadEvent::TimePoint when, const config::ConfigList& cfg)
{
    _snaps.emplace_back(make_unique<ProfileSnap>(when, *this, cfg));
    _procs.log(*_snaps.back());
}

config::ConfigList Thread::as_config() const
{
    config::ConfigList list;
    list["comm"] = comm();
    list["cmdline"] = cmdline();
    list["exe"] = fields().exe();
    list["environ"] = fields().environ();
    return list;
}

Process::Process(Thread& main_thread) :
_main_thread(main_thread)
{
}

Processes::Processes(TransitionLogger* logger) :
_logger(logger)
{
}

void Processes::add(Thread& thread)
{
    _all_threads[thread.glid()] = &thread;
    _active_threads[thread.tid()] = &thread;

    assert(thread_by_glid(thread.glid()) == &thread);
    assert(thread_by_tid(thread.tid()) == &thread);
}

void Processes::log(const ThreadEvent& trans)
{
    //work around const as we have access to the non-const map
    Thread& current{const_cast<Thread&>(trans.current())};

    switch(trans.type())
    {
    case ThreadEventType::FIRST:
    case ThreadEventType::FORK:
    case ThreadEventType::EXEC:
        add(current);
        break;

    case ThreadEventType::EXIT:
        current.demise(trans);
        _active_threads.erase(current.tid());
        break;
    }

    if (_logger)
        _logger->log(trans);
}

Process& Processes::process_create(Thread& main)
{
    Process* proc = new Process(main);
    return *(_procs.emplace(proc->glid(), proc).first->second);
}

const Thread* Processes::thread_by_glid(glid_t glid) const
{
    auto it {_all_threads.find(glid)};
    if (it == _all_threads.cend())
        return nullptr;
    return it->second;
}

const Thread* Processes::thread_by_tid(pid_t tid) const
{
    auto it {_active_threads.find(tid)};
    if (it == _active_threads.cend())
        return nullptr;
    return it->second;
}

Thread* Processes::thread_by_glid(glid_t glid)
{
    auto it {_all_threads.find(glid)};
    if (it == _all_threads.cend())
        return nullptr;
    return it->second;
}

Thread* Processes::thread_by_tid(pid_t tid)
{
    auto it {_active_threads.find(tid)};
    if (it == _active_threads.cend())
        return nullptr;
    return it->second;
}

void Processes::for_all(const std::function<void(const Thread&)>& f) const
{
    for (const auto& it : _all_threads)
        f(*it.second);
}
