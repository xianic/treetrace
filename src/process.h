#ifndef PROCESS_H
#define PROCESS_H

#include "configmap.h"
#include "glid.h"
#include "proc.h"

#include <sys/types.h>

#include <chrono>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

class ThreadFirst;
class Process;
class Processes;
class Thread;

enum class ThreadEventType
{
    FIRST = 0,
    FORK = 1,
    EXEC = 2,
    EXIT = 3,
    SNAP = 4
};

class ThreadEvent
{
public:
    typedef std::chrono::system_clock Clock;
    typedef std::chrono::time_point<Clock> TimePoint;

private:
    ThreadEventType _type;
    TimePoint _when;

public:
    ThreadEvent(ThreadEventType type, TimePoint when);
    virtual ~ThreadEvent();

    ThreadEventType type() const {return _type;}
    const TimePoint& when() const {return _when;}

    virtual Thread& current() = 0;
    virtual const Thread& current() const = 0;
};

struct ChainedThreadEvent : public ThreadEvent
{
    ChainedThreadEvent(ThreadEventType type, TimePoint when);

    virtual Thread& origin() = 0;
    virtual const Thread& origin() const = 0;
};

class TransitionLogger
{
public:
    virtual void log(const ThreadEvent&) = 0;
};

struct CommandLine : public std::vector<std::string>
{
    CommandLine(pid_t readProc);
    CommandLine(const std::string& nullSeperated);

    std::string bash_escape() const;
};

namespace config::str_helper
{
    template<>
    string as_string(const CommandLine& cmdline);

    template<>
    CommandLine from_string(const string&);
}

class ProfileSnap : public ThreadEvent
{
    static unsigned long _sc_clk_tck;

    Thread& _thread;
    unsigned long _utime;
    unsigned long _stime;
    ProcIO _io;
    std::string _cwd;

    ProfileSnap(ThreadEvent::TimePoint when, Thread&, const ProcStat&, const ProcIO&);

public:
    ProfileSnap(Thread&);
    ProfileSnap(ThreadEvent::TimePoint, Thread&, const config::ConfigList&);

    Thread& current() {return _thread;}
    const Thread& current() const {return _thread;}

    unsigned long utime() const {return _utime;}
    unsigned long stime() const {return _stime;}
    const std::string& cwd() {return _cwd;}

    config::ConfigList as_config() const;
};

class ProcessFields
{
    CommandLine _cmdline;
    std::string _comm;
    std::string _exe;
    std::string _environ;

public:
    ProcessFields(const ProcStat&, const bool capture_environ);
    ProcessFields(const ProcStat&, CommandLine&&, const bool capture_environ);
    ProcessFields(const config::ConfigList& list);

    CommandLine& cmdline() {return _cmdline;}
    const CommandLine& cmdline() const {return _cmdline;}

    std::string& comm() {return _comm;}
    const std::string& comm() const {return _comm;}

    std::string& exe() {return _exe;}
    const std::string& exe() const {return _exe;}

    std::string& environ() {return _environ;}
    const std::string& environ() const {return _environ;}
};

class Thread
{
public:
    typedef std::vector<std::unique_ptr<ProfileSnap>> snap_list_t;

private:
    const glid_t _glid;
    const pid_t _tid;

    snap_list_t _snaps;

    Processes& _procs;
    const ProcessFields& _fields;
    Process& _proc;

    const ThreadEvent& _origin;
    const ThreadEvent* _demise;

public:
    Thread(Processes& procs, ChainedThreadEvent& origin, pid_t tid, const ProcessFields& fields, bool new_process);
    Thread(Processes& procs, ChainedThreadEvent& origin, pid_t tid, const ProcessFields& fields, bool new_process, glid_t glid);
    Thread(Processes& procs, ThreadFirst& origin, pid_t tid, const ProcessFields& fields);
    Thread(Processes& procs, ThreadFirst& origin, pid_t tid, const ProcessFields& fields, glid_t glid);
    virtual ~Thread();

    Thread(const Thread&) = delete;
    Thread(Thread&&) = default;

    Thread& operator = (const Thread&) = delete;
    Thread& operator = (Thread&&) = default;

    glid_t glid() const {return _glid;}
    Process& process() {return _proc;}
    const Process& process() const {return _proc;}
    bool is_main() const;
    const Thread& main_thread() const;

    const ProcessFields& fields() const {return _fields;}
    pid_t tid() const {return _tid;}
    const CommandLine& cmdline() const {return _fields.cmdline();}
    const std::string& comm() const {return _fields.comm();}

    const ThreadEvent& origin() const {return _origin;}
    const ThreadEvent* demise() const {return _demise;}
    void demise(const ThreadEvent& demise);
    Processes& procs() const {return _procs;}

    void add_snap();
    void add_snap(ThreadEvent::TimePoint, const config::ConfigList&);
    const snap_list_t& snaps() const {return _snaps;}

    config::ConfigList as_config() const;
};

class ThreadFirst : public ThreadEvent
{
    ProcessFields _fields;
    Thread _current;

public:
    ThreadFirst(Processes&, TimePoint, pid_t tid, const bool capture_environ);
    ThreadFirst(Processes&, TimePoint, pid_t, glid_t, const config::ConfigList&);

    Thread& current() {return _current;}
    const Thread& current() const {return _current;}
};

class ThreadFork : public ChainedThreadEvent
{
    Thread& _origin;
    Thread _current;

public:
    ThreadFork(Processes&, TimePoint, Thread&, pid_t, bool new_process);
    ThreadFork(Processes&, TimePoint, Thread&, pid_t, bool new_process, glid_t);

    Thread& origin() {return _origin;}
    const Thread& origin() const {return _origin;}

    Thread& current() {return _current;}
    const Thread& current() const {return _current;}
};

class ThreadExec : public ChainedThreadEvent
{
    ProcessFields _fields;
    Thread& _origin;
    Thread _current;

public:
    ThreadExec(Processes&, TimePoint, Thread&, const bool capture_environ);
    ThreadExec(Processes&, TimePoint, Thread&, glid_t next, const config::ConfigList&);

    Thread& origin() {return _origin;}
    const Thread& origin() const {return _origin;}

    Thread& current() {return _current;}
    const Thread& current() const {return _current;}
};

class ThreadExit : public ThreadEvent
{
    Thread& _current;
    const bool _signaled;
    const bool _dumped;
    const int _code;

public:
    ThreadExit(Processes&, TimePoint, Thread&, int wstatus);
    ThreadExit(Processes&, TimePoint, Thread&, bool signaled, bool dumped, int code);

    Thread& current() {return _current;}
    const Thread& current() const {return _current;}

    bool signaled() const;
    int signal() const;
    const char* signal_desc() const;
    bool core_dumped() const;

    bool exited() const;
    int exit_code() const;
};

class Process
{
    std::vector<Thread*> _threads;
    Thread& _main_thread;

public:
    Process(Thread& main_thread);
    Process(const Process&) = delete;
    Process(Process&&) = default;

    Process& operator =(const Process&) = delete;
    Process& operator =(Process&&) = default;

    bool operator ==(const Process& that) const {return this->glid() == that.glid();}
    bool operator !=(const Process& that) const {return !(*this == that);}

    pid_t tgid() const {return _main_thread.tid();}
    glid_t glid() const {return _main_thread.glid();}

    const Thread& main_thread() const {return _main_thread;}
};

class Processes : public TransitionLogger
{
    std::vector<std::unique_ptr<const ThreadEvent>> _transitions;
    std::map<glid_t, std::unique_ptr<Process>> _procs;

    std::map<glid_t, Thread*> _all_threads;
    std::map<pid_t, Thread*> _active_threads;

    TransitionLogger* _logger;

    void add(Thread&);

public:
    Processes(TransitionLogger* upstream_logger = nullptr);

    Process& process_create(Thread& main_thread);

    Thread* thread_by_glid(glid_t glid);
    const Thread* thread_by_glid(glid_t glid) const;

    Thread* thread_by_tid(pid_t tid);
    const Thread* thread_by_tid(pid_t tid) const;

    void for_all(const std::function<void(const Thread&)>&) const;

    bool some_active() const {return !_active_threads.empty();}

    void log(const ThreadEvent& trans) override;

    template<typename Trans, typename... Args>
    Trans& transition(Args && ... args)
    {
        Trans* t = new Trans(*this, std::forward<Args>(args)...);
        _transitions.emplace(_transitions.end(), t);
        log(*t);
        return *t;
    }
};

#endif
