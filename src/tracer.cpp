#include "common.h"
#include "tracer.h"

#include <grp.h>
#include <pwd.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

namespace
{
    void switch_user(const string& user)
    {
        struct passwd* user_details = getpwnam(user.c_str());
        if (!user_details)
            throw_errno();

        struct group* group_details = getgrgid(user_details->pw_gid);
        if (!group_details)
            throw_errno();

        int glist_size = 8;
        vector<gid_t> glist(glist_size);
        int rt = getgrouplist(user.c_str(), user_details->pw_gid, glist.data(), &glist_size);
        if (rt == -1 && glist_size > glist.size())
        {
            glist.reserve(glist_size);
            while (glist.size() < glist_size)
                glist.push_back(0);
            rt = getgrouplist(user.c_str(), user_details->pw_gid, glist.data(), &glist_size);
        }
        if (rt == -1)
            throw runtime_error("getgrouplist");

        if (setgroups(glist.size(), glist.data()))
            throw_errno();

        if (setgid(group_details->gr_gid))
            throw_errno();

        if (setuid(user_details->pw_uid))
            throw_errno();
    }

    void ptrace_sieze(pid_t id)
    {
        if (-1 == ptrace(PTRACE_SEIZE, id, nullptr, nullptr))
            throw_errno();
    }

    unsigned long ptrace_geteventmsg(pid_t id)
    {
        unsigned long msg = 0;
        if (-1 == ptrace(PTRACE_GETEVENTMSG, id, nullptr, &msg))
            throw_errno();
        return msg;
    }

    void ptrace_cont(pid_t id, int signal)
    {
        if (-1 == ptrace(PTRACE_CONT, id, nullptr, signal))
        {
            // Occasionally we are processing events in a queue of a
            // process that has terminated. As such we will fail to make
            // it continue here. We will eventually see the termination
            // event and our view of the world will catch up. In the
            // mean time we have to ignore ESRCH and continue.
            if (errno != ESRCH)
                throw_errno();
        }
    }

    bool thread_is_of_proc(pid_t tid, pid_t tgid)
    {
        string file{"/proc/" + to_string(tgid) + "/task/" + to_string(tid)};
        int r = access(file.c_str(), 0);
        if (-1 != r)
            return true;
        switch (errno)
        {
        case ENOENT:
        case ENOTDIR:
            return false;

        default:
            throw_errno();
        }
    }

    pid_t fork_exec(char* argv[], const string& switch_to)
    {
        pid_t r = fork();
        switch (r)
        {
        case -1:
            throw_errno();
        case 0:
            break;
        default:
            return r;
        }

        if (!switch_to.empty())
            switch_user(switch_to);

        //stop here to allow the parent to PTRACE_SEIZE us. We will then be
        //sent SIGCONT
        if (-1 == raise(SIGSTOP))
            throw_errno();

        execvp(argv[0], argv);
        throw_errno();
    }

    class Tracer
    {
        struct Notification
        {
            pid_t id;
            int wstatus;
        };

        Processes& _procs;
        bool _init;
        const bool _capture_environ;

        void trap_fork(Thread& thread)
        {
            pid_t ntid = ptrace_geteventmsg(thread.tid());
            //PTRACE_GETEVENTMSG can be used to get new id
            ThreadFork& fork {_procs.transition<ThreadFork>(
                ThreadEvent::Clock::now(),
                thread,
                ntid,
                !thread_is_of_proc(ntid, thread.tid())
            )};
            fork.current().add_snap();
        }

        void trap_exec(Thread& thread)
        {
            //PTRACE_GETEVENTMSG gets old tid
            ThreadExec& exec {_procs.transition<ThreadExec>(ThreadEvent::Clock::now(), thread, _capture_environ)};
            exec.current().add_snap();
        }

        void trap_exit(Thread& thread)
        {
            thread.add_snap();
        }

        void trap(Thread& thread, int wstatus)
        {
            switch (wstatus >> 8)
            {
            case (SIGTRAP | PTRACE_EVENT_VFORK << 8):
            case (SIGTRAP | PTRACE_EVENT_FORK << 8):
            case (SIGTRAP | PTRACE_EVENT_CLONE << 8):
            case (SIGTRAP | PTRACE_EVENT_VFORK_DONE << 8):
                trap_fork(thread);
                break;

            case (SIGTRAP | PTRACE_EVENT_EXEC << 8):
                trap_exec(thread);
                break;

            case (SIGTRAP | PTRACE_EVENT_EXIT << 8):
                trap_exit(thread);

            /*case (SIGTRAP | 128 << 8):
            * Value of PTRACE_INTERRUPT. Unused even though some are observed.
            * Handled by default case.
            */

            default:
                //do nothing except fall through to continuing the process
                break;
            }

            ptrace_cont(thread.tid(), 0);
        }

        void init(Thread& thread)
        {
            if (-1 == ptrace(
                PTRACE_SETOPTIONS,
                thread.tid(),
                nullptr,
                PTRACE_O_TRACEEXEC | PTRACE_O_TRACECLONE | PTRACE_O_TRACEFORK | PTRACE_O_TRACEVFORK | PTRACE_O_TRACEEXIT
            ))
                throw_errno();
            ptrace_cont(thread.tid(), SIGCONT);
        }

        void event(Thread& thread, int wstatus)
        {
            if (WIFEXITED(wstatus) || WIFSIGNALED(wstatus))
                _procs.transition<ThreadExit>(ThreadEvent::Clock::now(), thread, wstatus);
            else if (WIFSTOPPED(wstatus))
            {
                int sig = WSTOPSIG(wstatus);
                if (sig == SIGTRAP)
                    trap(thread, wstatus);
                else if (_init && sig == SIGSTOP)
                {
                    init(thread);
                    _init = false;
                }
                else
                    ptrace_cont(thread.tid(), sig);
            }
            /* else if (WIFCONTINUED(wstatus))
                //this case shouldn't arrise as we are did not specify WCONTINUED to ::wait()
            */
        }

    public:
        Tracer(Processes& procs, bool capture_environ) :
        _procs(procs),
        _init(true),
        _capture_environ(capture_environ)
        {
        }

        void trace(pid_t child)
        {
            vector<Notification> notes;

            _procs.transition<ThreadFirst>(ThreadEvent::Clock::now(), child, _capture_environ);

            bool done;
            do
            {
                done = true;
                if (_procs.some_active())
                {
                    int wstatus = 0;
                    pid_t tid = waitpid(-1, &wstatus, 0);
                    if (tid == -1)
                        throw_errno();

                    notes.insert(notes.begin(), {tid, wstatus});
                    done = false;
                }

                auto it = notes.begin();
                while (it != notes.end())
                {
                    Thread* thread = _procs.thread_by_tid(it->id);
                    if(thread != nullptr)
                    {
                        event(*thread, it->wstatus);
                        it = notes.erase(it);
                        done = false;
                    }
                    else
                        ++it;
                }
            } while(!done);
        }
    };
}

void trace_exec(Processes& procs, char* argv[], const string& switch_to, const bool capture_environ)
{
    pid_t child = 0;

    try {
        child = fork_exec(argv, switch_to);
        ptrace_sieze(child);
    }
    catch(...)
    {
        if (child != 0)
            kill(child, SIGKILL);
        throw;
    }

    Tracer tracer(procs, capture_environ);
    tracer.trace(child);
}
