#ifndef _TRACER_H
#define _TRACER_H

#include "process.h"

#include <string>

void trace_exec(Processes&, char* argv[], const std::string& switch_to, const bool capture_environ);

#endif
