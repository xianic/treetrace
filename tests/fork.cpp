#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace std;

int main()
{
    pid_t child = fork();
    switch (child)
    {
    case -1:
        perror("fork");
        return 1;
    case 0:
        return 0;
    default:
        break;
    }

    child = wait(nullptr);
    if (-1 == child)
    {
        perror("wait");
        return 1;
    }

    return 0;
}
