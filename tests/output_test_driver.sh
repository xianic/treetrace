#!/bin/bash

set -e

if [ $# != 1 -o -z "$1" ]
then
    echo "usage: $(basename "$0") test" 1>&2
    exit 1
fi

NAME="$1"
OUT_DIR=.

TEST_DIR="$(dirname "$0")"

TREETRACE="$OUT_DIR/../src/treetrace"
NORMALISE="$TEST_DIR/normalise"
OUT_TREE="$OUT_DIR/$NAME.tree"
OUT_TRACE="$OUT_DIR/$NAME.trace"
OUT_DIFF="$OUT_DIR/$NAME.diff"
EXP_TREE="$TEST_DIR/$NAME.tree.expected"
EXP_TRACE="$TEST_DIR/$NAME.trace.expected"

echo "Executing treetrace"
"$TREETRACE" -t "$OUT_TREE" -l "$OUT_TRACE" -- "$OUT_DIR/$NAME.bin"

echo "Comparing results"
exec diff -u <("$NORMALISE" "$EXP_TRACE") <("$NORMALISE" "$OUT_TRACE") > "$OUT_DIFF"
