#include <chrono>
#include <thread>

using namespace std;

void work()
{
    this_thread::sleep_for(chrono::milliseconds(10));
}

int main()
{
    thread t1(work);
    work();
    t1.join();

    /*
     * Delay here means treetrace will see t1 exit well before the main
     * thread. This is important as the order of events will be compared
     * against expected values during the tests. Reordering events
     * would fail the test even if treetrace was behaving.
     */
    work();

    return 0;
}
