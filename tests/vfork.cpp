#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace std;

int main()
{
    const char* bin = "/bin/true";
    pid_t child = vfork();
    switch (child)
    {
    case -1:
        perror("fork");
        return 1;
    case 0:
        execl(bin, bin, nullptr);
        _exit(errno);
    default:
        break;
    }

    child = wait(nullptr);
    if (-1 == child)
    {
        perror("wait");
        return 1;
    }

    return 0;
}
